module.exports.isDriver = (req, res, next) => {
    const userRole = req.user.role;

    if (!userRole) {
        return res.status(401).json({
            message: 'Role is not defined'
        });
    }
    if (userRole === 'DRIVER') {
        next();
    } else {
        return res.status(401).json({
            message: 'Not authorized'
        });
    }
};

module.exports.isShipper = (req, res, next) => {
    const userRole = req.user.role;

    if (!userRole) {
        return res.status(401).json({
            message: 'Role is not defined'
        });
    }
    if (userRole === 'SHIPPER') {
        next();
    } else {
        return res.status(401).json({
            message: 'Not authorized'
        });
    }
};