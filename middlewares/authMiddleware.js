const {
    response
} = require('express');
const jwt = require('jsonwebtoken');
const {
    secret
} = require('../config/auth');

module.exports = (req, res, next) => {
    const authHeader = req.headers['authorization'];

    if (!authHeader) {
        return res.status(401).json({
            message: 'No authorization header found'
        });
    }

    const [, token] = authHeader.split(' ');

    try {
        req.user = jwt.verify(token, secret);
        next();
    } catch (err) {
        return response.status(401).json({
            message: 'jwt is invalid'
        });
    }
};