const {
    response
} = require('express');
const Log = require('../models/logModel');

module.exports = async (req, res, next) => {
    const log = new Log({
        request_method: req.method,
        request_body: req.body,
        request_url: req.url
    });

    try {
        await log.save();
        next();
    } catch (err) {
        response.json({
            message: err.message
        });
    }
};