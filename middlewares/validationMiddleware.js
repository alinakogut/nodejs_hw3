const Joi = require('joi');

module.exports = (schema) => {
    return async (req, res, next) => {
        const {
            error
        } = await schema.validate(req.body);

        if (error == null) {
            next();
        } else {
            const message = details.error.map((el) => el.message).join(',');
            res.status(422).json({
                error: message,
            });
        }
    };
};