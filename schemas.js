const Joi = require('joi');

module.exports = {
    changePasswordPatch: Joi.object().keys({
        oldPassword: Joi.string()
            .required(),
        newPassword: Joi.string()
            .required()
    }),
    addEditTruck: Joi.object().keys({
        type: Joi.string()
            .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
            .required()
    }),
    addLoadPost: Joi.object().keys({
        name: Joi.string()
            .required(),
        payload: Joi.number()
            .required(),
        pickup_address: Joi.string()
            .required(),
        delivery_address: Joi.string()
            .required(),
        dimensions: Joi.object().keys({
            width: Joi.number()
                .required(),
            height: Joi.number()
                .required(),
            length: Joi.number()
                .required()
        })
    }),
    editLoadPut: Joi.object().keys({
        name: Joi.string,
        payload: Joi.number(),
        pickup_address: Joi.string(),
        delivery_address: Joi.string(),
        dimensions: Joi.object().keys({
            width: Joi.number(),
            height: Joi.number(),
            length: Joi.number()
        })
    }),
    registerPost: Joi.object().keys({
        email: Joi.string().email({
            minDomainSegments: 2,
            tlds: {
                allow: ['com', 'net', 'ua']
            }
        }),
        password: Joi.string()
            .required(),
        role: Joi.string()
            .valid('SHIPPER', 'DRIVER')
            .required()
    }),
    loginPost: Joi.object().keys({
        email: Joi.string().email({
            minDomainSegments: 2,
            tlds: {
                allow: ['com', 'net', 'ua']
            }
        }),
        password: Joi.string().required(),
    })
};