const express = require('express');
const router = express.Router();
const logsMiddleware = require('../middlewares/logsMiddleware');
const schemas = require('../schemas');
const validationMiddleware = require('../middlewares/validationMiddleware');
const {
    register,
    login,
    forgotPassword
} = require('../controllers/authController');

router.post('/auth/register', logsMiddleware, validationMiddleware(schemas.registerPost), register);
router.post('/auth/login', logsMiddleware, login);
router.post('/auth/forgot_password', logsMiddleware, validationMiddleware(schemas.loginPost), forgotPassword);

module.exports = router;