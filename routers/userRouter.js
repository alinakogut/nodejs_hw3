const express = require('express');
const router = express.Router();
const logsMiddleware = require('../middlewares/logsMiddleware');
const authMiddleware = require('../middlewares/authMiddleware');
const schemas = require('../schemas');
const validationMiddleware = require('../middlewares/validationMiddleware');
const {
    getProfileInfo,
    changePassword,
    deleteProfile
} = require('../controllers/userController');

router.get('/users/me', logsMiddleware, authMiddleware, getProfileInfo);
router.patch('/users/me/password', logsMiddleware, validationMiddleware(schemas.changePasswordPatch), authMiddleware, changePassword);
router.delete('/users/me', logsMiddleware, authMiddleware, deleteProfile);


module.exports = router;