const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const logsMiddleware = require('../middlewares/logsMiddleware');
const schemas = require('../schemas');
const validationMiddleware = require('../middlewares/validationMiddleware');
const {
    isDriver
} = require('../middlewares/defineRoleMiddleware');

const {
    addTruck,
    getTrucks,
    getTruckById,
    assignTruck,
    updateTruck,
    deleteTruck
} = require('../controllers/truckController');

router.get('/trucks', logsMiddleware, authMiddleware, isDriver, getTrucks);
router.get('/trucks/:id', logsMiddleware, authMiddleware, isDriver, getTruckById);
router.put('/trucks/:id', logsMiddleware, validationMiddleware(schemas.addEditTruck), authMiddleware, isDriver, updateTruck);
router.post('/trucks', logsMiddleware, validationMiddleware(schemas.addEditTruck), authMiddleware, isDriver, addTruck);
router.post('/trucks/:id/assign', logsMiddleware, authMiddleware, isDriver, assignTruck);
router.delete('/trucks/:id', logsMiddleware, authMiddleware, isDriver, deleteTruck);


module.exports = router;