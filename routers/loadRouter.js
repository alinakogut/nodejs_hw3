const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');
const logsMiddleware = require('../middlewares/logsMiddleware');
const schemas = require('../schemas');
const validationMiddleware = require('../middlewares/validationMiddleware');
const {
    isShipper,
    isDriver
} = require('../middlewares/defineRoleMiddleware');


const {
    getLoads,
    addLoad,
    updateLoad,
    deleteLoad,
    getActiveLoad,
    iterateLoadState,
    getLoadById,
    postLoad,
    getShippingInfo
} = require('../controllers/loadController');

router.get('/loads', logsMiddleware, authMiddleware, getLoads);
router.get('/loads/active', logsMiddleware, authMiddleware, isDriver, getActiveLoad);
router.post('/loads', logsMiddleware, validationMiddleware(schemas.addLoadPost), authMiddleware, isShipper, addLoad);
router.patch('/loads/active/state', logsMiddleware, authMiddleware, isDriver, iterateLoadState);
router.get('/loads/:id', logsMiddleware, authMiddleware, isShipper, getLoadById);
router.put('/loads/:id', logsMiddleware, validationMiddleware(schemas.editLoadPut), authMiddleware, isShipper, updateLoad);
router.delete('/loads/:id', logsMiddleware, authMiddleware, isShipper, deleteLoad);
router.post('/loads/:id/post', logsMiddleware, authMiddleware, isShipper, postLoad);
router.get('/loads/:id/shipping_info', logsMiddleware, authMiddleware, isShipper, getShippingInfo);

module.exports = router;