module.exports = {
    port: process.env.PORT || process.env.APP_PORT,
    dbUrl: `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@node.hkv4s.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`
};