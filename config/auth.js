module.exports = {
    secret: process.env.JWT_SECRET,
    nodemailer_port: process.env.NODEMAILER_PORT || process.env.PORT,
    authCredentials: {
        user: process.env.AUTH_CREDENTIALS_USER,
        pass: process.env.AUTH_CREDENTIALS_PASS
    }
};