const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const variables = require('dotenv').config();

if (variables.error) {
    console.log(variables.error);
}

const app = express();

const {
    port,
    dbUrl
} = require('./config/server');

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const loadRouter = require('./routers/loadRouter');
const truckRouter = require('./routers/truckRouter');
const logRouter = require('./routers/logRouter');


mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
});

app.use(cors());
app.use(express.json());

app.use('/api', authRouter);
app.use('/api', userRouter);
app.use('/api', truckRouter);
app.use('/api', loadRouter);
// app.use('/api', logRouter);


app.listen(port, () => console.log(`Server is running on port ${port}...`));