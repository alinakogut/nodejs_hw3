# nodejs_hw3

## Description

Homework 3 - api for freight transport, created with Express and MongoDB.

## Requirements
Node.js. Installation link - https://nodejs.org

## Installation
```
$ git clone https://gitlab.com/alinakogut/nodejs_hw3
$ npm install
```
To run server user
```
$ npm start
```
## API documentation

Api documentation can be found in swagger.yaml. 