const mongoose = require('mongoose');

module.exports = mongoose.model('log', {
    time: {
        required: true,
        type: String,
        default: new Date()
    },
    request_method: {
        required: true,
        type: String
    },
    request_body: {
        required: true,
        type: Object
    },
    request_url: {
        required: true,
        type: String
    }
});