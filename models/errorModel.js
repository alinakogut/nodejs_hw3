const mongoose = require('mongoose');

module.exports = mongoose.model('error', {
    username: {
        required: true,
        type: String
    }
});