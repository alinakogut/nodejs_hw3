const mongoose = require('mongoose');

module.exports = mongoose.model('truck', {
    created_by: {
        required: true,
        type: String,
        default: new Date()
    },
    status: {
        type: String
    },
    type: {
        required: true,
        type: String
    },
    created_date: {
        required: true,
        type: String,
        default: new Date()
    },
    assigned_to: {
        type: String
    }
});