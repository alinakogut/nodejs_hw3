const mongoose = require('mongoose');

module.exports = mongoose.model('load', {
    created_by: {
        required: true,
        type: String
    },
    assigned_to: {
        type: String
    },
    status: {
        type: String,
        required: true
    },
    state: {
        type: String
    },
    name: {
        type: String,
        required: true
    },
    payload: {
        type: Number,
        required: true
    },
    pickup_address: {
        type: String,
        required: true
    },
    delivery_address: {
        type: String,
        required: true
    },
    dimensions: {
        type: Object,
        required: true,
        width: {
            type: Number,
            required: true
        },
        height: {
            type: Number,
            required: true
        },
        length: {
            type: Number,
            required: true
        }
    },
    logs: {
        type: Array
    },
    created_date: {
        required: true,
        type: String,
        default: new Date()
    }
});