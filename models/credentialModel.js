const mongoose = require('mongoose');

module.exports = mongoose.model('credential', {
    email: {
        required: true,
        type: String
    },
    password: {
        required: true,
        type: String
    }
});