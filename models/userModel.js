const mongoose = require('mongoose');

module.exports = mongoose.model('user', {
    email: {
        required: true,
        type: String
    },
    created_date: {
        required: true,
        type: String,
        default: new Date()
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true
    }
});