const url = require('url');
const Load = require('../models/loadModel');
const {
    lookForDriver
} = require('./truckController');
const {
    getTruck
} = require('./truckController');

const states = ['En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to'
];

function addLoadLog(loadId, logs, message) {
    logs.push({
        message,
        time: new Date()
    });
    return Load.findByIdAndUpdate(loadId, {
        $set: {
            logs
        }
    });
}

module.exports.getLoads = async (req, res) => {
    const userId = req.user._id;
    const userRole = req.user.role;
    const {
        query
    } = url.parse(req.url, true);

    try {
        const loads = userRole === 'DRIVER' ? await Load.find({
            assigned_to: userId,
            status: query.status || {
                $gte: ''
            }
        }).exec() : await Load.find({
            created_by: userId,
            status: query.status || {
                $gte: ''
            }
        }).exec();

        if (!loads) {
            return res.status(400).json({
                message: 'There is no any loads'
            });
        }

        res.json({
            loads
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};

module.exports.addLoad = async (req, res) => {
    const userId = req.user._id;

    const {
        dimensions,
        payload,
        pickup_address,
        delivery_address,
        name
    } = req.body;

    if (!dimensions ||
        !payload ||
        !pickup_address ||
        !delivery_address ||
        !name) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }

    const load = new Load({
        created_by: userId,
        logs: [{
            message: `Created by ${userId}, with status 'NEW'`,
            time: new Date()
        }],
        name,
        status: 'NEW',
        dimensions,
        payload,
        pickup_address,
        delivery_address
    });

    try {
        await load.save();
        res.json({
            message: 'Load created successfully'
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};


module.exports.getActiveLoad = async (req, res) => {
    const userId = req.user._id;
    try {
        const load = await Load.findOne({
            assigned_to: userId,
            status: 'ASSIGNED'
        }).exec();

        if (!load) {
            return res.status(400).json({
                message: 'Load not found'
            });
        }
        res.json({
            load
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};

module.exports.getLoadById = async (req, res) => {
    const loadId = req.params.id;
    const userId = req.user._id;

    if (!loadId) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }

    try {
        const load = await Load.findOne({
            _id: loadId,
            created_by: userId
        }).exec();
        if (!load) {
            return res.status(400).json({
                message: 'Load not found'
            });
        }
        res.json({
            load
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};


module.exports.updateLoad = async (req, res) => {
    const loadId = req.params.id;
    const userId = req.user._id;

    if (!loadId) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }
    try {
        const load = await Load.findOneAndUpdate({
            _id: loadId,
            created_by: userId,
            status: 'NEW'
        }, {
            $set: req.body
        }).exec();
        if (!load) {
            return res.status(400).json({
                message: 'Load not found'
            });
        }
        await addLoadLog(load._id, load.logs,
            `Load was update by shipper with id ${userId}`);
        res.json({
            message: 'Load details changed successfully'
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};

module.exports.deleteLoad = async (req, res) => {
    const loadId = req.params.id;
    const userId = req.user._id;

    if (!loadId) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }

    try {
        await Load.findOneAndDelete({
            _id: loadId,
            created_by: userId,
            status: 'NEW'
        }).exec();
        res.json({
            message: 'Load deleted successfully'
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};

module.exports.postLoad = async (req, res) => {
    const loadId = req.params.id;
    const userId = req.user._id;

    if (!loadId) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }
    try {
        const load = await Load.findOneAndUpdate({
            _id: loadId,
            created_by: userId,
            status: 'NEW'
        }, {
            status: 'POSTED'
        }).exec();

        const truck = await lookForDriver(load.dimensions, load.payload);
        if (!truck) {
            await Load.findOneAndUpdate({
                _id: loadId,
                created_by: userId
            }, {
                status: 'NEW'
            });
        } else {
            await Load.findOneAndUpdate({
                _id: loadId,
                created_by: userId
            }, {
                status: 'ASSIGNED',
                assigned_to: truck.assigned_to,
                state: states[0]
            });
        }
        addLoadLog(load._id, load.logs,
            `Load was posted by shipper with id ${userId}`);
        res.json({
            message: 'Load posted successfully',
            driver_found: !!truck,
            truck: truck
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};


module.exports.getShippingInfo = async (req, res) => {
    const loadId = req.params.id;
    const userId = req.user._id;

    if (!loadId) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }

    try {
        const load = await Load.findOne({
            _id: loadId,
            created_by: userId,
            status: 'ASSIGNED'
        }).exec();
        const truck = await getTruck(load.assigned_to);

        if (!load || !truck) {
            res.status(400).json({
                message: 'Incorrect data'
            });
        }

        res.json({
            load,
            truck
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};

module.exports.iterateLoadState = async (req, res) => {
    const loadId = req.params.id;
    const userId = req.user._id;

    try {
        const load = await Load.findOne({
            assigned_to: userId
        }).exec();

        if (!load) {
            return res.status(400).json({
                message: 'There is no such load'
            });
        }

        const currentStateIndex = states.indexOf(load.state || '');
        const currentState = currentStateIndex === -1 ? 0 : currentStateIndex;

        await Load.findOneAndUpdate({
            _id: loadId,
            assigned_to: userId
        }, {
            state: states[currentState + 1]
        }).exec();

        addLoadLog(load._id, load.logs,
            `Load state was changed to ${states[currentState+1]}
         by user width id ${userId}`);

        res.json({
            message: `Load state changed to ${states[currentState+1]}`
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};