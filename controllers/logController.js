const Log = require('../models/logModel');
const {
    response
} = require('express');

module.exports = async () => {
    try {
        const logs = await Log.find().exec();

        if (!logs) {
            return response.status(400).json({
                message: 'There is no any logs'
            });
        }
        response.json({
            logs
        });
    } catch (err) {
        response.status(500).json({
            message: err.message
        });
    }
};