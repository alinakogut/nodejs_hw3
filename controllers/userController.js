const User = require('../models/userModel');

module.exports.getProfileInfo = async (req, res) => {
    const userId = req.user._id;

    try {
        const user = await User.findById(userId).exec();
        if (!user) {
            return res.status(400).json({
                message: 'There is no such user'
            });
        }
        res.json({
            user
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};

module.exports.changePassword = async (req, res) => {
    const userId = req.user._id;
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;

    if (!userId) {
        return res.status(400).json({
            message: 'There is no such user'
        });
    }

    try {
        const user = await User.findById(userId).exec();
        if (!user) {
            return res.status(400).json({
                message: 'There is no such user'
            });
        }
        if (!oldPassword || !newPassword || oldPassword !== user.password) {
            return res.status(400).json({
                message: 'Incorrect data'
            });
        }
        await User.updateOne({
            _id: userId
        }, {
            password: req.body.newPassword
        }).exec();

        res.json({
            message: 'Password changed successfully'
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};

module.exports.deleteProfile = async (req, res) => {
    const userId = req.user._id;

    if (!userId) {
        return res.status(400).json({
            message: 'There is no such user'
        });
    }

    try {
        const user = await User.findOneAndDelete({
            _id: userId
        }).exec();
        if (!user) {
            return res.status(400).json({
                message: 'There is no such user'
            });
        }
        res.json({
            message: 'Profile deleted successfully'
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};