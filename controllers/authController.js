const jwt = require('jsonwebtoken');
const User = require('../models/userModel');
const Bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');

const {
    secret,
    authCredentials
} = require('../config/auth');

module.exports.register = async (req, res) => {
    const {
        email,
        role
    } = req.body;
    let {
        password
    } = req.body;

    if (!email || !password || !role) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }
    try {
        password = await Bcrypt.hash(password, 10);
        await new User({
            email,
            password,
            role
        }).save();
        res.json({
            message: 'Profile created successfully'
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};

module.exports.login = async (req, res) => {
    const {
        email,
        password
    } = req.body;

    if (!email || !password) {
        return res.status(400).json({
            message: 'Enter username and password'
        });
    }

    try {
        const user = await User.findOne({
            email
        }).exec();
        if (!user) {
            return res.status(400).json({
                message: 'Wrong email'
            });
        }
        const isCorrectPass = await Bcrypt.compare(password, user.password);

        if (!isCorrectPass) {
            return res.status(400).json({
                message: 'Wrong password'
            });
        }
        res.json({
            role: user.role,
            jwt_token: jwt.sign(JSON.stringify(user), secret)
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};


module.exports.forgotPassword = async (req, res) => {
    const userEmail = req.body.email;
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: authCredentials
    });
    try {
        const newPassword = `${req.url.slice(-2)}${new Date()
            .getTime()}${req.url.slice(6, 8)}`;
        await User.findOneAndUpdate({
            email: userEmail
        }, {
            password: await Bcrypt.hash(newPassword, 10)
        });
        await transporter.sendMail({
            from: 'uber.like.app.testing@gmail.com',
            to: userEmail,
            subject: 'New password',
            html: `<b>Your new password:</b> ${newPassword} 
            Don't forget to change it.`
        });
        res.json({
            message: 'New password was sent to your email'
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};