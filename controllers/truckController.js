const Truck = require('../models/truckModel');

module.exports.getTrucks = async (req, res) => {
    const userId = req.user._id;

    try {
        const trucks = await Truck.find({
            created_by: userId
        }).exec();

        if (!trucks) {
            return res.status(400).json({
                message: 'There is no any trucks'
            });
        }
        res.json({
            trucks
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};

module.exports.getTruckById = async (req, res) => {
    const userId = req.user._id;
    const truckId = req.params.id;
    try {
        const truck = Truck.find({
            created_by: userId,
            _id: truckId
        }).exec();

        if (!truck) {
            return res.status(400).json({
                message: 'There is no such truck'
            });
        }
        res.json({
            truck
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};
// /////////////////////////////////////////////////
module.exports.getTruck = (assigned_to) => {
    return Truck.find({
        assigned_to
    }).exec();
};
// /////////////////////////////////////////////////

module.exports.addTruck = async (req, res) => {
    const userId = req.user._id;
    const {
        type
    } = req.body;

    if (!type) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }

    const truck = new Truck({
        created_by: userId,
        type
    });
    try {
        await truck.save();
        res.json({
            message: 'Truck created successfully'
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};

module.exports.updateTruck = async (req, res) => {
    const truckId = req.params.id;
    const userId = req.user._id;

    if (!truckId) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }
    try {
        const truck = await Truck.findOneAndUpdate({
            _id: truckId,
            created_by: userId,
            assigned_to: {
                $exists: false
            }
        }, {
            $set: req.body
        }).exec();
        if (!truck) {
            return res.status(400).json({
                message: 'There is no such truck'
            });
        }
        res.json({
            message: 'Truck details changed successfully'
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};

module.exports.deleteTruck = async (req, res) => {
    const truckId = req.params.id;
    const userId = req.user._id;

    if (!truckId) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }

    try {
        const truck = await Truck.findOneAndDelete({
            _id: truckId,
            created_by: userId,
            assigned_to: {
                $exists: false
            }
        }).exec();
        if (!truck) {
            return res.status(400).json({
                message: 'There is no such truck'
            });
        }
        res.json({
            message: 'Truck deleted successfully'
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};

module.exports.assignTruck = async (req, res) => {
    const truckId = req.params.id;
    const userId = req.user._id;

    if (!truckId) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }
    try {
        const truck = Truck.findOneAndUpdate({
            _id: truckId,
            created_by: userId
        }, {
            $set: {
                assigned_to: userId,
                status: 'IS'
            }
        }).exec();

        if (!truck) {
            return res.status(400).json({
                message: 'There is no such truck'
            });
        }

        res.json({
            message: 'Truck assigned successfully'
        });
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
};

module.exports.lookForDriver = (dimensions, payload) => {
    let type = 'SPRINTER';

    if (dimensions.width >= 500 && dimensions.width < 700 &&
        dimensions.height >= 250 && dimensions.height < 350 &&
        dimensions.length >= 170 && dimensions.length < 200 &&
        payload <= 4000) {
        type = 'LARGE STRAIGHT';
    } else if (dimensions.width >= 300 && dimensions.width < 500 &&
        dimensions.height < 250 &&
        dimensions.length < 200 &&
        payload <= 2500) {
        type = 'SMALL STRAIGHT';
    }

    return Truck.findOneAndUpdate({
        status: 'IS',
        type
    }, {
        status: 'OL'
    }).exec();
};